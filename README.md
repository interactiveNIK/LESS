
LESS TEMPLATE GUIDE

//////////////////////////////

BACKGROUND IMAGE / GRADIENT
------------------------------
background: url($theme + '/images/photo-gallery-banner.png') no-repeat right center / contain, 
radial-gradient(ellipse at 25% center,#e1e1e1 1%,#a1a29d 95%) center;

